﻿(*
  ITT8060 -- Advanced Programming 2015
  Department of Computer Science
  Tallinn University of Technology
  ------------------------------------
  Coursework 1: Tuples, functions, basic pattern matching
  ------------------------------------
  Name: Omisakin Oluwatobi
  Student ID: olomis
  ------------------------------------
  Answer the questions below.  You answers to questions 1--7 should be
  correct F# code written after the question. This file is an F#
  script file, it should be possible to load the whole file at
  once. If you can't then you have introduced a syntax error
  somewhere.
  This coursework will be graded. It has to be submitted to the TUT git system using
  the instructions on the course web page by September 25, 2015.
*)

// 1. Make an instance of a tuple with 3 elements, where all elements are of different types.

let tupData = ("Omisakin", 55, 5.0)

// 2. Make an instance of a tuple of type int * int * int * int .

let tupdata2 = (22, 22, 34, 55)

// 3. Make a function that takes two arguments, an integer and a five element tuple of integers
// that returns true if the sum of any three elements of the tuple is greater than the first argument, else false.
//let greatTuple (number:int) tupple =
//    match number 
//    | when (_,23,44,55,55)

let greatTuple (a:int,b:int,c:int,d:int,e:int) (num:int) =
    let v = a + b + c
    let w = a + b + d
    let x = a + b + e
    let y = a + c + d
    let z = a + c + e
    let o = b + c + d
    let l = b + c + e
    let mm = b + d + e
    let nn = c + d + e
    let s = a + d + e
    match num with
    | abc when (abc<v || abc<w || abc<x || abc<y || abc<z || abc<s || abc<o || abc<l || abc<mm || abc<nn) -> true
    | _ -> false

//let res = greatTuple (2,3,1,1,1) 7
//res

// 4. Make a function that takes a tuple of five integers and returns a tuple of five integers where at the 
// position of the largest element would be the result of division of the largest element with the smallest one greater than one.

//let BIGTup tupleri =
//    let max = 0
//    match tupleri with
//    | (a,b,c,d) when a>0 

let DivGreatRep (v:int, w:int, x:int, y:int,z:int) =
  let less =
    if (v<w && v<x && v<y && w<z) then v
    else if (w<v && w<x && w<y && w<z) then w
    else if (x<v && x<w && x<y && x<z) then x
    else if (y<v && y<w && y<x && y<z) then y
    else if (z<v && z<w && z<x && z<y) then z
    else 0
  let big =   
    if (v>w && v>x && v>y && w>z) then v
    else if (w>v && w>x && w>y && w>z) then w
    else if (x>v && x>w && x>y && x>z) then x
    else if (y>v && y>w && y>x && y>z) then y
    else if (z>v && z>w && z>x && z>y) then z
    else 0
  match (v,w,x,y,z) with
  | (a,b,c,d,e) when (a>=big && a>1) -> (a/less, b,c,d,e)
  | (a,b,c,d,e) when (b>=big && b>1) -> (a,b/less,c,d,e)
  | (a,b,c,d,e) when (c>=big && c>1) -> (a,b,c/less,d,e)
  | (a,b,c,d,e) when (d>=big && d>1) -> (a,b,c,d/less,e)
  | (a,b,c,d,e) when (e>=big && e>1) -> (a,b,c,d,e/less)
  | _ -> (v,w,x,y,z)


   

// 6. Make 3 functions that return the average of the value of elements in
// tuples containing 2, 3, or 4 elements of float type.
//let  tupi = 44.0, 23.0, 66.0, 12.0

let aveVal (a, b, c, d) = 
    match (a, b, c, d) with
    | (w, x, y, z)-> (w + x + y + z)/4.0

let aveVal2 (a, b, c) = 
    match (a, b, c) with
    | (w, x, y)-> ((w + x + y)/3.0)

let aveVal3 (a, b) = 
    match (a, b) with
    | (w, x) -> (w + x)/2.0


// 7. Write 3 functions 'allcaps2', 'allcaps3' and 'allcaps4' which capitalize
// the strings in a tuple of strings where the size of the tuple is either 2,3, or 4.
// Hint: "abc".ToUpper ()
let allCaps (a:string,b:string) =
    match (a,b) with
    |(c,d) -> (c.ToUpper(),d.ToUpper())

let allCaps2 (a:string,b:string, c:string) =
    match (a,b,c) with
    |(x,y,z) -> (x.ToUpper(),y.ToUpper(),z.ToUpper())

let allCaps3 (a:string, b:string, c:string, d:string) =
    match (a,b,c,d) with
    |(w,x,y,z) -> (w.ToUpper(),x.ToUpper(),y.ToUpper(),z.ToUpper())