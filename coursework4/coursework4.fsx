(*
  ITT8060 -- Advanced Programming 2015
  Department of Computer Science
  Tallinn University of Technology
  ------------------------------------
  Coursework 4: Higher order functions, option, list
  ------------------------------------
  Name: Omisakin Oluwatobi Samuel
  Student ID: omitobi
  ------------------------------------
  Answer the questions below. You answers to the questions should be
  correct F# code written after the question. This file is an F# script
  file; it should be possible to load the whole file at once. If you
  can't, then you have introduced a syntax error somewhere.
  This coursework will be graded.
  Commit and push your solution to the repository as file
  coursework4.fsx in directory coursework4.
  The deadline for completing the above procedure is Friday,
  October 23, 2015.
  We will consider the submission to be the latest version of the
  appropriate files in the appropriate directory before the deadline
  of a particular coursework.
*)

// 1. Write a function by pattern matching
// Some (None)
//   flattenOption : option<option<'a>> -> option<'a>
//
//   which squashes two layers of possible successes or failures into 1
//   E.g. Some Some 1 -> Some 1
let flattenOption (made:option<option<'a>>):option<'a> =
    match made with
    | None -> None
    | Some (None) -> None
    | Some (Some a) -> Some a

// 2. Can flattenOption by implemented using bind? If so, do it!

let flattenOption2 make  =  Option.bind (fun a-> a) make

// 3. Write a function
//
//    defeatist : list<option<'a>> -> option<list<'a>>
//
//    that takes a list of possible successes or failures and returns
//    a list of successes if everything succeeded or returns failure
//    if 1 or more elements of the list was a failure. Again, pay
//    close attention to the type.
//    E.g. [Some 1 ; Some 2] -> Some [1; 2]


let defeatist (list:list<option<'a>>):option<list<'a>> = 
    let rec newDefeat list  = 
        match list with
        |[] -> []
        | None::t -> newDefeat t
        | Some h::t -> h::newDefeat t
    let newList = newDefeat list
    match newList with
    |[] -> None
    |hh::tt -> Some(hh::tt)



// 4. Write a function
//
//    optimist : 'a -> list<option<'a>> -> list<'a>
//
//    which collects a list of possible successes or failures into a
//    list containing only the successes with all failures replaced
//    by the first parameter of the function. Pay close attention to the type.
//    E.g. optimist 0 [Some 1; None] -> [1; 0]

let rec optimist j list=
    match list with
    | [] -> []
    | None::tail -> j::(optimist j tail)
    | Some a::tail -> a::(optimist j tail)


// 5. Write a function
//
//    chars : list<string> -> list<char>
//
//    This function should use List.collect (bind) and have the
//    following behaviour:
//    ["hello";"world"] -> ['h';'e';'l';'l';'o';'w';'o';'r';'l';'d']        

let chars (list: string List)=  list |>  List.collect (fun x -> x.ToCharArray() |> List.ofSeq)
let chars2 (strings:string):list<char> =[for i in strings -> if i=' ' then ' ' else i  ]
chars2 "ABC D"
// 6. Write a function
//
//    iprint : list<int> -> string
//
//    This function should use List.foldBack and have the following behaviour:
//    [1 .. 5] |-> "1,2,3,4,5,"

let iprint (list:int List) =
    match list with
    | [] -> ""
    |_::_ -> List.foldBack (fun xt ab -> xt.ToString()+ "," + ab) list ""

