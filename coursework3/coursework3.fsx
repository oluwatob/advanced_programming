﻿(*
  ITT8060 -- Advanced Programming 2015
  Department of Computer Science
  Tallinn University of Technology
  ------------------------------------
  Coursework 3: User defined types
  ------------------------------------
  Name: Omisakin Oluwatobi
  TUT Student ID: olomis
  ------------------------------------
  Answer the questions below.  You answers to questions 1--7 should be
  correct F# code written after the question. This file is an F#
  script file, it should be possible to load the whole file at
  once. If you can't then you have introduced a syntax error
  somewhere.
  This coursework will be graded. It has to be submitted to the TUT
  git system using the instructions on the course web page by October 9, 2015.
*)


// 1. Make a function rearrange: Fexpr -> Fexpr that will rearrange a finite expression tree
// (as defined in the lecture in the differentiation example) in such a way that constants
// always are to the left of variables in addition and multiplication and the variables with
// higher power in multiplication are pushed to the right.
// Add(X, Const 1.0) -> Add(Const 1.0, X)
// Mul(X, Const 1.0) -> Mul(Const 1.0, X)
//Add
//    (Mul (Const 0.0,Mul (Mul (X,X),X)),
//     Mul
//       (Const 2.0,
//        Add
//          (Mul (Const 1.0,Mul (X,X)),
//           Mul (X,Add (Mul (Const 1.0,X),Mul (X,Const 1.0))))))
// -> 
//Add
//    (Mul (Const 0.0,Mul (X,Mul (X,X))),
//     Mul
//       (Const 2.0,
//        Add
//          (Mul (Const 1.0,Mul (X,X)),
//           Mul (X,Add (Mul (Const 1.0,X),Mul (Const 1.0,X))))))
type Fexpr = |Const of float
             |X
             |Add of Fexpr * Fexpr
 //            |Sub of Fexpr * Fexpr
             |Mul of Fexpr * Fexpr
//             |Div of Fexpr * Fexpr
             
let rec D (fe:Fexpr) =
    
    match fe with
    |Const _        -> Const 0.0
    |X              -> Const 1.0
    |Add(fe1, fe2) -> if D fe1 = Const 0.0
                       then Add( fe2,  fe1)
                        elif D fe1>fe2
                         then Add( fe2,  fe1)
                         else Add( fe1,  fe2)
    |Mul(fe1, fe2) -> Add(Mul(D fe2,fe1), Mul(fe2,D fe1))



// 2. Make a function simplify: Fexpr -> Fexpr that will simplify a finite expression tree by removing
// terms that evaluate to zero.
//
// For example:
// Add (Const 0.0, X) -> X
// Mul (Const 1.0, X) -> X
//Add
//    (Mul (Const 0.0,Mul (X,Mul (X,X))),
//     Mul
//       (Const 2.0,
//        Add
//          (Mul (Const 1.0,Mul (X,X)),
//           Mul (X,Add (Mul (Const 1.0,X),Mul (Const 1.0,X))))))
// ->
//     Mul
//       (Const 2.0,
//        Add
//          (Mul (X,X),
//           Mul (X,Add (X,X)))


// 3-4: Given the type definition:
// type BList =
//  | BEmpty
//  | Snoc of BList * int
// 

type BList =
    | BEmpty
    | Snoc of BList * int
// 3. Make the function filterB: (prop: int -> bool) BList -> BList that will return a list for the elements of which
// the function prop returns true.

let rec filterB(prop: int -> bool) list =
    match list with
    | BEmpty              -> BEmpty
    | Snoc(theList, tail) -> let filteredList = filterB prop theList
                             if prop tail
                                then Snoc (filteredList, tail)
                                   else filteredList

// 4. Make the function mapB: (trans: int -> int) BList -> BList that will return a list where the function trans has
// been applied to each element.

let rec mapB (trans: int -> int) list =
    match list with
    | BEmpty                 -> BEmpty
    | Snoc (up,down)         -> Snoc (mapB trans up, trans down)

// 5-7. Given the type definition
// type Tree =
//  | Nil
//  | Branch2 of Tree * int * Tree
//  | Branch3 of Tree * int * Tree * int * Tree
// 
// 5. Define the value exampleTree : Tree that represents the following
//    tree:
//
//        2
//       / \
//      *  3 5
//        / | \
//       *  *  *
type Tree =
  | Leaf
  | Node2 of Tree * int * Tree
  | Node3 of Tree * int * Tree * int * Tree

let exampleTree = Node2(Leaf,2,Node3(Leaf,3,Leaf,5,Leaf))

//let t1 = Leaf
//let t2 = Node2(Leaf, 2, Leaf)
//let tr = (t2, 3, Node2, 5, Node3)
//let t3 = Node2(t1,1,t2)
//let t4 = Node3(t1,4,t2,5,t3)
// 6. Define a function sumTree : Tree -> int that computes the sum of
//    all labels in the given tree.

let rec sumTree tree = 
    match tree with
    | Leaf                                    -> 0
    | Node2 (tree1,two,tree2)                 -> sumTree tree1 + sumTree tree2 + two 
    | Node3 (tree1, val1, tree2, val2, tree3) -> sumTree tree1 + sumTree tree2 + sumTree tree3 + val1 + val2

sumTree exampleTree

// 7. Define a function productTree : Tree -> int that computes the
//    product of all labels in the given tree. If this function
//    encounters a label 0, it shall not look at any further labels, but
//    return 0 right away.

// ** Bonus questions **

(*let productTree Tree =
    match Tree with
    |0 -> 0
*)

let rec productTree tree = 
  match tree with 
  | Leaf                                    -> 1
  | Node2 (tree1,mid,tree2)                 -> if mid <> 0
                                                then productTree tree1 * mid * productTree tree2
                                                    else 0
  | Node3 (tree1, val1, tree2, val2, tree3) -> if val1 <> 0 || val2 <> 0
                                                then productTree tree1 * val1 * productTree tree2 *  val2 * productTree tree3
                                                    else 0

productTree exampleTree

// 8. Define a function mapTree : (int -> int) -> Tree -> Tree that
//    applies the given function to every label of the given tree.

let rec mapTree (lf:int->int) tree =
    match tree with
    | Leaf                                -> Leaf
    | Node2 (tr1, mid, tr2)               -> Node2 (mapTree lf tr1, lf mid, mapTree lf tr2)
    | Node3 (tr1, leaf1, tr2, leaf2, tr3) -> Node3 (mapTree lf tr1, lf leaf1, mapTree lf tr2, lf leaf2, mapTree lf tr3)

// 9. Use mapTree to implement a function negateAll : Tree -> Tree that
//    negates all labels of a given tree.

let neg = (*)-1
let negateAll tree = mapTree (neg) exampleTree

