﻿// Learn more about F# at http://fsharp.org. See the 'F# Tutorial' project
// for more guidance on F# programming.

#load "Library1.fs"
open CorrectRomanNumbers

// Define your library scripting code here
let romanDivider (theLetters:string) =
    let theDiv = [for i in theLetters -> i]
    let rec romanDiv theDiv =
        match theDiv with
        |[] -> failwith("Empty string is not allowed")
        |x::xs -> let rm = romanDiv xs
                  match x with
                  |' ' -> failwith("Empty string is not allowed")
                  | 'I' -> 1
                  | 'V' -> 5
                  | 'X' -> 10
                  | 'L' -> 50
                  | 'C' -> 100
                  | 'D' -> 500
                  | 'M' -> 1000
    romanDiv theDiv
    

romanDivider "V"    
        
let romanConverter (theLetters:string) =
    match theLetters with
    |"" -> [failwith("Not accepted")]
    |a -> [for i in a -> 
            match a with
            |i -> 0
            ]