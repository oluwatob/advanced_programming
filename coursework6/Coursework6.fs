﻿module File1


(*

  ITT8060 -- Advanced Programming 2015
  Department of Computer Science
  Tallinn University of Technology
  ------------------------------------------------

  Coursework 6: Testing

  ------------------------------------------------
  Name: Omisakin Oluwatobi
  Student ID: olomis
  ------------------------------------------------


  Answer the questions below. You answers to the questions should be correct F#
  code written after the question. This file is an F# script file; it should be
  possible to load the whole file at once. If you can't, then you have
  introduced a syntax error somewhere.

  This coursework will be graded.

  Commit and push your script part of the solution to the repository as
  file coursework6.fsx in directory coursework6.

  The file that should be compiled to a dll should go into coursework6.fs.

  Please do not upload DLL-s. Just include a readme.txt file containing the 
  dependencies required (additional DLLs)

  The deadline for completing the above procedure is Friday, November 13, 2015.

  We will consider the submission to be the latest version of the appropriate
  files in the appropriate directory before the deadline of a particular
  coursework.

*)

(*
     One of your former colleagues wrote some code to convert Roman numbers to Arabic
     numbers. After writing this code he left for greener pastures at a new
     and cool startup.

     You are now left to make sure the code he wrote works and can be shipped to the
     customer, one of the largest banks in the region and the most important one to 
     your company. 
*)

(*   1) You are given the code in file coursework6input/Library1.fs that can be
     compiled to BrokenRomanNumbers.dll.
     You are expected to test the code using NUnit or FsCheck unit tests (at least 6 in total).
*)

open NUnit.Framework
open FsUnit

type convertNumber (numString) =
    member me.theString = numString
    member me.result = convert(me.theString) //XCIX should not throw an exception
    
//let convObj = new convert("XYZ")
[<TestFixture>] 
    type ``Given any String of Roman Numerals`` ()=
        let testlist = ["XM";"XD";"XXXX";"IIII";"XD";"IXC";"LL"]
        let convObj = new convertNumber("IV")
        let convObj1 = new convertNumber("XXXX")// checks for more than 3X's
        let convObj2 = new convertNumber("XD")
        let convObj3 = new convertNumber("XM")
        let convObj4 = new convertNumber("IXC")
        let convObj5 = new convertNumber("MMMMM") //tries to see the if it returns more than 3999
        let convObj6 = new convertNumber("LL")
        let convObj7 = new convertNumber("") //vhecks for empty string
        let convObj8 = new convertNumber("IIII")
        let convObj9 = new convertNumber("XCIX") //should print 99
        let convObj10 = new convertNumber("i") //for lowercase

        [<Test>] member x.
         ``When the string is supplied, Not more than 3 X can be allowed `` ()=
                convObj1.result |> should equal (instanceOfType<System.Exception>)

         [<Test>] member x.
         ``Return exception for result that would be more than 3999`` ()= 
                 convObj5.result |>  should not' (be greaterThanOrEqualTo 4000)      

        [<Test>] member x.
                ``With empty string, should expect an exception.`` () =
                convObj7.result |> should equal (instanceOfType<System.Exception>)

        [<Test>] member x.
                ``Should also accept lowercase and convert it without an exception`` () =
                convObj10.result |> should not' (equal (instanceOfType<System.Exception>))

        [<Test>] member x.
                ``Should convert the string XCIX to 99`` () =
                    convObj9.result |> should equal 99

         [<Test>] member x.``If incorrect roman numerals entered, should throw exception``() =
                        try
                           let result = convert("XD") //an example of the incorrect roman numerals
                           let result = convert("XM")
                           let result = convert("IIII")
                           let result = convert("LL")
                           Assert.Fail()
                         with
                        | :? AssertionException -> failwith("Expected an exception, but none was shown.")
                        | _ -> ()




(*   2) List faults discovered as a table. Please try to shrink the inputs
     to the smallest samples corresponding to the appropriate fault.

RomanNumber ExpectedOutput OutputFromConvert FoundWithFsCheckOrNUnit
*)
(*
RomanNumber     ExpectedOutput OutputFromConvert FoundWithFsCheckOrNUnit
MMMMM         | not >= 4000   |  5000          | Expected: not greater than or equal to 4000  But was:  5000
"XCIX"        | 990           |  InvalidInput  | Expected: 990, but was <instanceof System.Exception>
"XXXX"        | Exception     | 40             | Expected: <instanceof System.Exception>, but was 40
""            | Exception     | 0              | Expected: <instanceof System.Exception>, but was 0
i             | 1             | Invalid input  | System.Exception : invalid input
XD,XM,"IIII"  | Exception     | 490, 990, 4    | Expected and exception, but none was shown.

*)

(*   3) Write a CorrectRomanNumbers implementation in functional style that you
     would be confident to include in mission critical applications. Test the
     implementation using the tests defined previously.
*)


