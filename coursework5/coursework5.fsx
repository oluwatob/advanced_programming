(*
  ITT8060 -- Advanced Programming 2015
  Department of Computer Science
  Tallinn University of Technology
  ------------------------------------
  Coursework 5: Records, List.collect and Charting
  ------------------------------------
  Name: Omisakin Oluwatobi Samuel
  Student ID: olomis
  ------------------------------------
  Answer the questions below. You answers to the questions should be
  correct F# code written after the question. This file is an F# script
  file; it should be possible to load the whole file at once. If you
  can't, then you have introduced a syntax error somewhere.
  This coursework will be graded.
  Commit and push your solution to the repository as file
  coursework5.fsx in directory coursework5. The downloaded data should go in
  atoms.xml in the coursework5 directory.
  The deadline for completing the above procedure is Friday,
  October 30, 2014.
  We will consider the submission to be the latest version of the
  appropriate files in the appropriate directory before the deadline
  of a particular coursework.
*)
// 1) Define a record type with the name FileMetaData representing the name
//    of the file and the size of it. 

type FileMetaData =
    {
    Name: string;
    Size : int64
    }

// 2) Make a function that takes a directory as input  
//    and produces a list of instances of the FileMetaData record type defined in Q 1.
open System.IO

//let directory = "C:\\tp\\images"

let fileInstance theDirectory = theDirectory                            //theDirectory is a single file, so a string type is good enough
                                |> Directory.GetFiles 
                                |> List.ofSeq 
                                |> List.map (fun f -> 
                                    {Name= Path.GetFileNameWithoutExtension f; 
                                        Size = (FileInfo f).Length})


// 3) Make a function called getFileMetadata of type string list -> FileMetaData list
//    that takes a list of directories as strings and returns a list of FileMetaData records of
//    all files contained in the directories. You are encouraged to use List.collect
//    in the solution
let getFileMetaData directory = 
    match directory with
    | []    -> []
    | _     -> directory |> List.collect(fileInstance)  //I used the function "fileInstance" created in question 2
getFileMetaData ["C:\\tp\\images"; "C:\\tp"]

// 4) Make a function that displays a histogram chart showing the distribution of
//    the file sizes given a list of FileMetaData records.

#load @"..\packages\FSharp.Charting.0.90.12\FSharp.Charting.fsx"
#r @"..\packages\FSharp.Charting.0.90.12\lib\net40\FSharp.Charting.dll"

open FSharp.Charting
open FSharp.Charting.ChartTypes


//alternative way to load library

//List.countBy
let dispHisto (metaData:FileMetaData List) = 
    match metaData with
    |_   -> Chart.Column(
                            [for x in metaData ->x.Size], 
                            Name ="The Directory Files", 
                            Title = "The Files Histogram", 
                            Labels = [for x in metaData ->x.Name]).ShowChart()
dispHisto (getFileMetaData ["C:\\tp\\images"; "C:\\tp"])