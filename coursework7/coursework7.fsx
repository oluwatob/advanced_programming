(*

  ITT8060 -- Advanced Programming 2015
  Department of Computer Science
  Tallinn University of Technology
  ------------------------------------------------

  Coursework 7: Generating data for testing

  ------------------------------------------------
  Name: Omisakin Oluwatobi
  Student ID: olomis
  ------------------------------------------------


  Answer the questions below. You answers to the questions should be correct F#
  code written after the question. This file is an F# script file; it should be
  possible to load the whole file at once. If you can't, then you have
  introduced a syntax error somewhere.

  This coursework will be graded.

  Commit and push your solution to the repository as
  file coursework7.fsx in directory coursework7.

  Please do not upload any DLL-s. Just include a readme.md file containing the 
  dependencies required (additional DLLs)

  The deadline for completing the above procedure is Friday, November 20, 2015.

  We will consider the submission to be the latest version of the appropriate
  files in the appropriate directory before the deadline of a particular
  coursework.

*)

(*
You should now have a BrokenRomanNumbers implementation and your own CorrectRomanNumbers
implementation from coursework 6.
*)

(*   1) Write at least 3 FsCheck properties that you can use to check both
     implementations of Roman numeral conversions. The properties should return
     "OK" in the case of correct implementation, and "Falsifyable" in the
     broken implementation.
*)

//concat functions to concat strings sent in by the number of concat specified
let rec concat (str:string) xs:string =
    let result = match xs with
                 | 0 -> ""   
                 | _ -> str + concat str (xs-1)
    result

//convert_to_roman function is adapted from "arabicToRoman" function on http://fsharpforfunandprofit.com/posts/roman-numeral-kata/
let convert_to_roman num = 
    let theNumChecker theNumLoc =
      match theNumLoc with
      | ([],[]) -> ""
      | (numNLoc, htu) ->
      let theLoc = numNLoc.Head
      let theNum = numNLoc.Item(1)
      let numres1 = theNum % (10*theLoc)
      let numres2 = numres1/theLoc
      let the1s = htu.Head
      let the2s = htu.Item(1)
      let the3s = htu.Item(2)
      let result = match numres2 with
                    | 0 -> ""
                    | 1 -> the1s
                    | 2 -> concat the1s 2
                    | 3 -> concat the1s 3
                    | 4 -> the1s + the2s 
                    | 5 -> the2s
                    | 6 -> the2s + the1s
                    | 7 -> the2s + concat the1s 3
                    | 8 -> the2s + concat the1s 1
                    | 9 -> the1s + the3s  
                    | _ -> failwith "Wrong Entry!"
      result
    let first = ([1 ; num], ["I";"V";"X"]) |> theNumChecker
    let second =  ([10 ; num], ["X";"L";"C"]) |> theNumChecker
    let third =  ([100 ; num], ["C";"D";"M"]) |> theNumChecker
    let fourth =  ([1000 ; num], ["M";"";"?"]) |> theNumChecker
    fourth + third + second + first
//This is the CorrectRomanNumber function that converts from roman letters to arabic numerals
let roman_converter (theString:string) =
    let theCorrectList = [for i in 1..3999 -> (convert_to_roman i).ToLower()]
    let theLowerString = theString.ToLower()
    let eachRomanList = [for chars in theLowerString -> chars]
    if List.exists(fun a -> a = theLowerString) theCorrectList then
        let rec converter num1 num2 theNumChar =
            match theNumChar with
            | x::xs ->
                let theVal = match x with
                                | 'm' -> 1000
                                | 'd' -> 500
                                | 'c' -> 100
                                | 'l' -> 50
                                | 'x' -> 10
                                | 'v' -> 5
                                | 'i' -> 1
                                | _ -> failwith ("invalid input !!!!")
                let op = if theVal > num2 then (-) else (+) 
                xs |> converter (num2 |> op num1 ) theVal
            | _ -> (+) num1 num2
        (eachRomanList) |> converter 0  0 
    else 
        failwith("Invalid Input from the beginning!")
///////////////////////////////////////////////////////////////////////////////
#r @"..\packages\FsCheck.2.2.2\lib\net45\FsCheck.dll"
open FsCheck

let createStringFromStringList (cs : string list) = 
            gen {
            let! chars = Gen.arrayOf (Gen.elements cs)
            return System.String.Concat(chars)
            }

let theAllowed = [for i in 1..3999 -> (convert_to_roman i)] //Sets the max limit of roman numerals
let isMBad= ["M"]//match n>0 with | true -> [for i in 1..n -> concat "M" i] |_ -> ["M"]
let mCheck = ["M";"MM";"MMM"] //sets only one to three M
let theOrder = ["X";"I";"V";"L";"C";"D";"M"]//(List.collect(fun f -> [for i in 1..3 -> (concat f i)] @ ["V";"L";"D"])["I";"X";"C";"M"])
let theSmallOrder = [for i in theOrder -> i.ToLower()] //sets the order to lowercase

//The First property
let therightorder =  
    Prop.forAll(Arb.fromGen (createStringFromStringList theOrder))(fun f -> 
            if (List.exists(fun k -> k=f ) theAllowed) then
               // Prop.ofTestable((convert (f)).GetType() = typeof<int>)        //Uncomment for testing the broken one
                Prop.ofTestable((roman_converter (f)).GetType() = typeof<int>) //Uncomment to test the Correct Roman
            else
               // Prop.throws(lazy convert (f))                                 //Uncomment for testing the broken one
                Prop.throws(lazy roman_converter (f))                           //Uncomment to test the Correct Roman
)
//The Second property
let thelowercase =  
    Prop.forAll(Arb.fromGen (createStringFromStringList theSmallOrder))(fun f -> 
            if (List.exists(fun (k:string) -> k.ToLower()=f ) theAllowed) then
               // Prop.ofTestable((convert (f)).GetType() = typeof<int>)       //Uncomment for testing the broken one
                Prop.ofTestable((roman_converter (f)).GetType() = typeof<int>) //Uncomment to test the Correct Roman
            else
               // Prop.throws(lazy convert (f))             //Uncomment for testing the broken one
                Prop.throws(lazy roman_converter (f))       //Uncomment to test the Correct Roman
)
//The Third property
let morethan3999 =  
    Prop.forAll(Arb.fromGen (createStringFromStringList isMBad))(fun f -> 
            if (List.exists(fun k -> k=f) mCheck) then
                //Prop.ofTestable((convert (f)).GetType() = typeof<int>)    //Uncomment for testing the broken one
                Prop.ofTestable((roman_converter (f)).GetType() = typeof<int>) //Uncomment to test the Correct Roman
            else
                //Prop.throws(lazy convert (f))                                 //Uncomment for testing the broken one
                Prop.throws(lazy roman_converter (f))                           //Uncomment to test the Correct Roman
)

Check.Verbose therightorder
Check.Verbose thelowercase
Check.Verbose morethan3999



(*   2) Write at least 1 data generator and at least 1 FsCheck property for the following code:

type Client = 
  { Name : string; Income : int ; YearsInJob : int
    UsesCreditCard : bool;  CriminalRecord : bool }

type QueryInfo =
  { Title     : string
    Check     : Client -> bool
    Positive  : Decision
    Negative  : Decision }

and Decision = 
   | Result of string
   | Query  of QueryInfo

let rec tree =
   Query  {Title = "More than �40k"
           Check = (fun cl -> cl.Income > 40000)
           Positive = moreThan40
           Negative = lessThan40}
and moreThan40 =
   Query  {Title = "Has criminal record"
           Check = (fun cl -> cl.CriminalRecord)
           Positive = Result "NO"
           Negative = Result "YES"}
and lessThan40 =
   Query  {Title = "Years in job"
           Check = (fun cl -> cl.YearsInJob > 1)
           Positive = Result "YES"
           Negative = usesCreditCard}
and usesCreditCard =
   Query  {Title = "Uses credit card"
           Check = (fun cl -> cl.UsesCreditCard)
           Positive = Result "YES"
           Negative = Result "NO"}

let rec testClientTree client tree =
    match tree with
    | Result msg  -> printfn " OFFER A LOAN: %s" msg ; msg // Added that msg gets returned, otherwise side effects are hard to test.
    | Query qinfo -> let result, case = 
                         if qinfo.Check(client) then
                             "yes", qinfo.Positive
                         else
                             "no", qinfo.Negative
                     printfn " - %s ? %s" qinfo.Title result
                     testClientTree client case
*)
//Requires FsCheck.dll and "open FsCheck"
//The Generator
type clientGenerator = 
        static member clientArb =
            Arb.generate<Client> 
            |> Gen.map (fun a -> { Client.Name = a.Name; 
                                    Income=a.Income; 
                                    YearsInJob=a.YearsInJob;
                                    UsesCreditCard=a.UsesCreditCard;
                                    CriminalRecord=a.CriminalRecord})
//The property
let clientProperty client decision =
        if (client.Income < 0 || client.YearsInJob<0) then // MY TEST IS THAT: income and years CANNOT be realistically less than 0
            Prop.throws( lazy (testClientTree client decision)) //Expecting an exception [ofcourse no exception in the given code up there
        elif obj.Equals((testClientTree client decision),null)=true then //Checks for null result
            ((testClientTree client decision).GetType() = typeof<string>) ==> false
         else
            ((testClientTree client decision).GetType() = typeof<string>) ==> true
