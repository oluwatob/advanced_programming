(*

  ITT8060 -- Advanced Programming 2015
  Department of Computer Science
  Tallinn University of Technology
  ------------------------------------------------

  Coursework 8: Sequences and computation expressions

  ------------------------------------------------
  Name: Omisakin Oluwatpbi Samuel
  Student ID: olomis
  ------------------------------------------------


  Answer the questions below. You answers to the questions should be correct F#
  code written after the question. This file is an F# script file; it should be
  possible to load the whole file at once. If you can't, then you have
  introduced a syntax error somewhere.

  This coursework will be graded.

  Commit and push your solution to the repository as file coursework8.fsx in
  directory coursework8.

  The deadline for completing the above procedure is Friday, December 4, 2015.

  We will consider the submission to be the latest version of the appropriate
  files in the appropriate directory before the deadline of a particular
  coursework.

*)

// 1. Make a declaration for the sequence of odd numbers.
Seq.initInfinite(fun f -> f) |> Seq.filter(fun f -> (f%2=1)) 

// 2. Declare a function that, for given i and n, selects the sublist [a_i;a_i+1;...;a_i+n-1]
//    of sequence [a_0; a_1; ...]
let afunction i n seqlist  = seq[for i in i..(i+(n-1)) -> (Seq.item(i) seqlist)]

// 3. Use the functions in the Seq library to declare a function cartesian sqx sqy that gives
// a sequence containing all pairs (x,y) where x is a member of sqx and y is a member of sqy.
let cartesian sqx sqy =
    sqx |> Seq.collect(fun x ->(sqy |> Seq.collect(fun y -> seq{yield (x,y)})))
// 4. Make an alternative solution to 3 using sequence expressions.
let altercartesian sqx sqy =
    match sqx with
    | _-> seq{
                    for x in sqx do
                        match sqy with
                        | _ -> for y in sqy do
                                yield (x,y)
                        } 
// 5. Extend the logging workflow explained in the lecture to support downloading web pages.
// Log the http response code for each URL. Make a function that takes a sequence of URLs and 
// downloads them while logging the response codes using the logging workflow.
#r @"..\packages\Http.fs.1.5.1\lib\net40\HttpClient.dll"
open HttpClient  //The nugget package for HttpClient.dll must be added to get the Response code
open Logging     // The Logging Module is also required to use the Logging classes

type requestResponseLogger(urlSeq) =
    member this.urlSequence = urlSeq
    member this.urlResponceCodeLogger =
         this.urlSequence|> Seq.map (fun url -> createRequest Get url |> getResponseCodeAsync)
        |> Async.Parallel
        |> Async.RunSynchronously
    member this.theReturn =
                this.urlResponceCodeLogger |> Array.toSeq |> Seq.map (fun f -> logMessage("Writing..."+f.ToString())) //Logging the Response code to Logger while downloading.
let weblist =["http://www.google.com";"http://www.yahoo.com";"http://trans-prime.com"] //the sequence url to test
let testResponseLogger = new requestResponseLogger(weblist) //an instance of the testResponseLogger
testResponseLogger.theReturn //testing